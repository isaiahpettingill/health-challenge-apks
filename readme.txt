Health Challenge App

FAQ
Q. What is this?

A. The Red Pepper Health challenge, previously paper, now put in app form.
    This is the first official release.

Q. Which version do I choose?

A. Short Answer
    I don't know

A. Long answer
    Supposedly the arm64-v8a is the newest version. If you have a really new phone, try that one.

    armeabi-v7a is going to be the most common. If you have a mainstream, not brand-new, non-obscure phone, this will be the app for you.

    x86_64 is a release just in case you have one of these phones. If neither of the other apps work, try that one.

    app.apk was generated by flutter. If absolutely none of these apps work, install it.

Q. Can I get this on iOS

A. Yes. An iOS version is available at https://testflight.apple.com/join/63ZRUrPF
